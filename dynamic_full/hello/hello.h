#pragma once

#ifdef __cplusplus
extern "C" {
#endif

namespace hello
{
	void sayHello();
}

#ifdef __cplusplus
}
#endif

