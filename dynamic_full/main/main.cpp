#include <iostream>
#include <dlfcn.h>

int main()
{
	void * libHandle = dlopen("hello.so", RTLD_LAZY);
	
	if(!libHandle)
	{
		std::cerr << "Failed to load hello.so : " << dlerror() << std::endl;
		return 1;
	}

	typedef void (*funcPtr) (void); 

	funcPtr sayHello = (funcPtr) dlsym(libHandle, "sayHello");

	char * error = dlerror();

	if(error != NULL)
	{
		std::cerr << "Failed to load sayHello() : " << error << std::endl;
		return 1;
	}

	(*sayHello)();

	dlclose(libHandle);

	return 0;
}

