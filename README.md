Libraries on Linux
==================

This repo contains the three types of librairies described in http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html
* static: 
	The library code is integrated into the final executable
* dynamic_static: 
	The library code is not integrated into the executable but shared. The library must be available at compile (static awareness, the headers msut be included) and run time.
* dynamic_full:
	Same as above but there is no static awareness: the headers need not be included, the code is loaded at run time using the dynamic loader (dlopen, dlsym...)

To list the files bundled in a library archive, use:
ar t xxx.a

To list the functions exported by a library or executable, use:
nm xxx --demangle

T	The symbol is in the text (code) section.
U	The symbol is undefined.

V	The symbol is a weak object.
W	The symbol is a weak symbol that has not been specifically tagged as a weak object symbol.

R	The symbol is in a read only data section.
